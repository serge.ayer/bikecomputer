#!/bin/bash

EXTENSION_TO_CHECK=(".cpp" ".h")
PATH_TO_CHECK=("TB/Code/cellular/source" "TB/Code/lorawan/source")
MBED_PATH=".hooks/.astylerc"

# variable to check if there are files formatted or not during this operation
file_formatted=0

for filename in $(git diff --name-only --cached)
do
    for extension in "${EXTENSION_TO_CHECK[@]}"
    do
        if [[ "$filename" == *"$extension" ]]; then
            for path in "${PATH_TO_CHECK[@]}"
            do
                if [[ "$filename" == *"$path"* ]]; then
                    if [[ $(astyle -n --options=$MBED_PATH $filename | grep "Formatted") ]]; then
                        echo "Formatted $filename"
                        #file_formatted=1 && break  #uncomment this line to fail the commit if a file is commited
                        $(git add $filename)
                        break
                    fi
                fi
            done
        fi
    done
done

# return 1 (an error) if there are files formatted. --> Need to readd files and then commit
exit $file_formatted

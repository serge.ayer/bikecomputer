#pragma once

#include "mbed.h"

namespace rtc {

class WheelCounterDevice {
public:
  WheelCounterDevice();

  // method called for getting the current wheel rotation count
  int getCurrentRotationCount();

  // method called for resetting the counter
  void reset();

  // definition of task period time
  static constexpr std::chrono::milliseconds TASK_PERIOD = 400ms;

private:
  // definition of task execution time
  static constexpr std::chrono::microseconds TASK_RUN_TIME = 200000us;
  // definition of wheel rotation time
  static constexpr std::chrono::milliseconds WHEEL_ROTATION_TIME = 200ms;

  // data members 
  LowPowerTicker m_ticker;
  int m_rotationCount = 0;
  
  // private methods
  void turn();
};

} // namespace

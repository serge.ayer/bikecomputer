#pragma once

#include "mbed.h"

namespace rtc {

class LCDDisplay {
public:

  LCDDisplay();

  // method called for displaying information
  void show(int currentGear, int currentRotationCount);

  // definition of task period time
  static constexpr std::chrono::milliseconds TASK_PERIOD = 1600ms;
  
private:
  // data members  
  // definition of task execution time
  static constexpr std::chrono::microseconds TASK_RUN_TIME = 300000us;
};

} // namespace

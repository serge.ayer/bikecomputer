#pragma once

namespace single_implementation {

struct ProcessedData {
    float averageSpeed = 0;
    float averagePower = 0;
};

} // namespace

  
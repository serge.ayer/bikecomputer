#pragma once

#include "config.h"
#include "GearSystemDevice.h"
#include "WheelCounterDevice.h"
#include "ResetDevice.h"
#include "LCDDisplay.h"
#include "ProcessedData.h"

#include "TaskInformationTable.h"

namespace single_implementation {

class BikeSystem {
public:
#if defined(USE_STATIC_SCHEDULING)
  BikeSystem(Timer& timer);
#elif defined(USE_RTC)
  BikeSystem(Timer& timer, TaskInformationTable& taskInformationTable);
#elif defined(USE_EVENT_QUEUE) 
  BikeSystem(Timer& timer, EventQueue& eventQueue);
#elif defined(USE_MULTITASKING)
  BikeSystem();
#endif

#if defined(USE_MULTITASKING)
  void start();
#else 
  void updateCurrentGear();
  void updateWheelRotationCount();
  
  void checkAndPerformReset();
  void updateDisplay(int subTaskIndex);
  void updateDisplayInOneTask();
#endif

private:
#if defined(USE_EVENT_HANDLING)
  // private methods
  void setReset();
  void setNewGear();
#endif

  // number of wheel rotation (read from the device)
  int m_wheelRotationCount = 0; 
  // current gear (read from the device)
  int m_gear = 0;
  // data member that represents the device for manipulating the gear
  GearSystemDevice m_gearSystemDevice;
  // data member that represents the device for counting wheel rotations  
  WheelCounterDevice m_wheelCounterDevice;
  // data member that represents the device used for resetting
  ResetDevice m_resetDevice;
  // data member that represents the device display
  LCDDisplay m_lcdDisplay;
  
  // data used for ISR for push button
#if defined(USE_EVENT_HANDLING)
  volatile bool m_reset = false;
#endif
 
  // data used for ISR for gear system
#if defined(USE_EVENT_HANDLING)
  volatile bool m_newGear = false;
#endif

#if defined (USE_RTC)
  // used in ISRs for setting task state
  TaskInformationTable& m_taskInformationTable;
#endif

#if defined (USE_EVENT_QUEUE)
  // used in ISRs for deferring event handling job to the queue
  EventQueue& m_eventQueueForISRs;
#endif

  // used for performance measurements
#if defined(USE_MULTITASKING)
  // wheel circumference in mm
  static const int WHEEL_CIRCUMFERENCE = 2200;
  Queue<int, COUNT_QUEUE_SIZE> m_countQueue;
  Mail<ProcessedData, PROCESSED_DATA_QUEUE_SIZE> m_processedMail;
  Thread m_processingThread;
  EventQueue m_eventQueueForISRs;
  Timer m_timer;
  void processData();
#else
  void logPeriodAndExecutionTime(int taskIndex, const std::chrono::microseconds& taskStartTime);
  Timer& m_timer;
  static const int NBR_OF_TASKS = 4;
  static const int RESET_TASK_INDEX = 0;
  static const int GEAR_TASK_INDEX = 1;
  static const int COUNT_TASK_INDEX = 2;
  static const int DISPLAY_TASK_INDEX = 3;
  static const char* TASK_DESCRIPTORS[];
  std::chrono::microseconds m_taskStartTime[NBR_OF_TASKS];
#endif
  std::chrono::microseconds m_resetTime;
};

} // namespace

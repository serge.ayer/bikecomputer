#include "GearSystemDevice.h"

namespace single_implementation {

// initialization of static variables
const std::chrono::milliseconds GearSystemDevice::READING_RATE = 1000ms;
const std::chrono::microseconds GearSystemDevice::TASK_RUN_TIME = 100000us;

#if defined(USE_EVENT_HANDLING)
GearSystemDevice::GearSystemDevice(mbed::Callback<void()> cb) :
  m_usbSerial(true) {
  
  // attach the callback to the serial port
  m_usbSerial.attach(cb);
}
#else
GearSystemDevice::GearSystemDevice() :
  m_usbSerial(true) {
  // start the thread for reading information from the usb serial
  m_thread.start(callback(this, &GearSystemDevice::read));
}
#endif

#if ! defined(USE_EVENT_HANDLING)
void GearSystemDevice::read() {
  while (true) {
    if (m_usbSerial.available()) {
      // printf("Got character %c\n", m_usbSerial.getc());
      char str[2] = { (char) m_usbSerial.getc(), 0};
      m_currentGear = atoi(str);
    }
    ThisThread::sleep_for(READING_RATE);
  }
}
#endif

int GearSystemDevice::getCurrentGear() {
  // when a callback is registered, we need to read data
  // on the serial port upon getting the current gear
#if defined(USE_EVENT_HANDLING)
  while (m_usbSerial.available()) {
    // printf("Got character %c\n", m_usbSerial.getc());
    char str[2] = { (char) m_usbSerial.getc(), 0};
    m_currentGear = atoi(str);
  }
#endif

  // simulate task computation by waiting for the required task run time
  wait_us(TASK_RUN_TIME.count());
  return m_currentGear;
}

} // namespace

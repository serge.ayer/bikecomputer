#include "LCDDisplay.h"

namespace single_implementation {

// initialization of static variables
const std::chrono::microseconds LCDDisplay::TASK_RUN_TIME = 300000us;
const std::chrono::microseconds LCDDisplay::SUBTASK_RUN_TIME = 100000us;
const std::chrono::milliseconds LCDDisplay::TASK_PERIOD = 1600ms;

#if defined(USE_MULTITASKING)
LCDDisplay::LCDDisplay(Mail<ProcessedData, PROCESSED_DATA_QUEUE_SIZE>& processedMail) :
  m_processedMail(processedMail) {
}
#else
LCDDisplay::LCDDisplay() {
}
#endif

#if defined(USE_MULTITASKING)
void LCDDisplay::start() {
  // start a thread that will display information
  m_thread.start(callback(this, &LCDDisplay::displayInfo));

  printf("LCDDisplay started\n");
}

void LCDDisplay::displayInfo() {
  printf("LCDDisplay thread started\n");
  while (true) {
    ProcessedData* pProcessedData = m_processedMail.try_get_for(Kernel::wait_for_u32_forever);
    float speed = pProcessedData->averageSpeed;
    printf("Average speed is %d.%d\n", (int) speed, (int) ((speed - (int) speed) * 100000));
    m_processedMail.free(pProcessedData);    
  }
}
#else
void LCDDisplay::show(int currentGear, int currentRotationCount, int subTaskIndex) {
  if (subTaskIndex == -1) {
    // simulate task computation by waiting for the required task run time
    wait_us(TASK_RUN_TIME.count());
    printf("Gear value is %d, wheel rotation count is %d\n", currentGear, currentRotationCount);
  } 
  else {    
    // simulate task computation by waiting for the required task run time
    wait_us(SUBTASK_RUN_TIME.count());
    if (subTaskIndex == (NBR_OF_SUBTASKS - 1)) {
      printf("Gear value is %d, wheel rotation count is %d\n", currentGear, currentRotationCount);
    }
  }
}
#endif

} // namespace

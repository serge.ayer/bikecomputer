#pragma once

#include "mbed.h"

#include "config.h"

#include "ProcessedData.h"

namespace single_implementation {

class LCDDisplay {
public:

#if defined(USE_MULTITASKING)
  LCDDisplay(Mail<ProcessedData, PROCESSED_DATA_QUEUE_SIZE>& processedMail);
#else
  LCDDisplay();
#endif

#if defined(USE_MULTITASKING)
  void start();
#else
  // method called for displaying information
  void show(int currentGear, int currentRotationCount, int subTaskIndex = -1);
#endif

  // definition of task period time
  static const std::chrono::milliseconds TASK_PERIOD;
  
private:
  // data members
  // definition of task execution time
  static const std::chrono::microseconds TASK_RUN_TIME;
  static const std::chrono::microseconds SUBTASK_RUN_TIME; 

  // for simulating a task run time, we sleep for this time in the show() method
  
  static const int NBR_OF_SUBTASKS = 3;

#if defined(USE_MULTITASKING)
  void displayInfo();
  Mail<ProcessedData, PROCESSED_DATA_QUEUE_SIZE>& m_processedMail;
  Thread m_thread;
#endif
};

} // namespace


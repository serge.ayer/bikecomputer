#include "WheelCounterDevice.h"

namespace single_implementation {
  
// initialization of static variables
const std::chrono::microseconds WheelCounterDevice::TASK_RUN_TIME = 200000us;
const std::chrono::milliseconds WheelCounterDevice::TASK_PERIOD = 400ms;
const std::chrono::milliseconds WheelCounterDevice::WHEEL_ROTATION_TIME = 1000ms;

#if defined(USE_MULTITASKING)
WheelCounterDevice::WheelCounterDevice(Queue<int, COUNT_QUEUE_SIZE>& countQueue) :
  m_countQueue(countQueue) {
}
#else
WheelCounterDevice::WheelCounterDevice() {
  // start the ticker for incrementing wheel counter
  m_ticker.attach(callback(this, &WheelCounterDevice::turn), WHEEL_ROTATION_TIME);
}
#endif

#if defined(USE_MULTITASKING)
void WheelCounterDevice::start() {
  // initialize random seed
  srand (time(NULL));

  // start a ticker for signaling a wheel rotation
  m_ticker.attach(callback(this, &WheelCounterDevice::turn), WHEEL_ROTATION_TIME);
  printf("WheelCounterDevice started\n");
}
#else
int WheelCounterDevice::getCurrentRotationCount() {
  // simulate task computation by waiting for the required task run time
  wait_us(TASK_RUN_TIME.count());
  return m_rotationCount;
}

void WheelCounterDevice::reset() {
  m_rotationCount = 0;
}
#endif

void WheelCounterDevice::turn() {
  // ISR context
#if defined(USE_MULTITASKING)
  m_rotationCount++;
  if (m_countQueue.try_put((int*) m_rotationCount)) {
    m_rotationCount--;
  } 
#else
  // increment rotation count
  // the call for incrementing the count should be atomic
  m_rotationCount++;
#endif
}

} // namespace

#pragma once

#include "mbed.h"

#include "config.h"

namespace single_implementation {
  
class ResetDevice {
public:
#if defined(USE_EVENT_HANDLING)
  // constructor used for event-driven behavior
  ResetDevice(mbed::Callback<void()> cb);
#else
  // constructor 
  ResetDevice();
#endif

#if !defined(USE_EVENT_HANDLING)
  // method called for checking the reset status
  bool checkReset();
#endif

private:
  // definition of task execution time
  static const std::chrono::microseconds TASK_RUN_TIME;

  // reading rate in milliseconds when running a separate thread
  // The gear value is updated every second
  static const std::chrono::milliseconds READING_RATE;
  
  // data members  
  // instance representing the reset button
  InterruptIn m_resetButton;
};

} // namespace

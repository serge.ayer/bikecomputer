#pragma once

#include "mbed.h"

#include "config.h"

namespace single_implementation {
  
class WheelCounterDevice {
public:
#if defined(USE_MULTITASKING)
  WheelCounterDevice(Queue<int, COUNT_QUEUE_SIZE>& countQueue);
#else
  WheelCounterDevice();
#endif

#if defined(USE_MULTITASKING)
  void start();
#else
  // method called for getting the current wheel rotation count
  int getCurrentRotationCount();

  // method called for resetting the counter
  void reset();
#endif

  // definition of task period time
  static const std::chrono::milliseconds TASK_PERIOD;
  

private:
  // definition of task execution time
  static const std::chrono::microseconds TASK_RUN_TIME; 
  // definition of wheel rotation time
  static const std::chrono::milliseconds WHEEL_ROTATION_TIME;

  // data members 
  LowPowerTicker m_ticker;
#if defined(USE_MULTITASKING) 
  Queue<int, COUNT_QUEUE_SIZE>& m_countQueue;
#endif    
  int m_rotationCount = 0;
  
  // private methods
  void turn();
};

} // namespace

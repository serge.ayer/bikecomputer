#include "BikeSystem.h"

namespace single_implementation {
  
// initialization of static variables
#if !defined(USE_MULTITASKING)
const char* BikeSystem::TASK_DESCRIPTORS[] = { "Reset", "Gear", "Count", "Display"};
#endif
#if defined(USE_STATIC_SCHEDULING)
BikeSystem::BikeSystem(Timer& timer) :
  m_timer(timer)
#if defined(USE_EVENT_HANDLING)
  , m_resetDevice(callback(this, &BikeSystem::setReset))
  , m_gearSystemDevice(callback(this, &BikeSystem::setNewGear)) 
#endif 
{
}
#elif defined(USE_RTC)
BikeSystem::BikeSystem(Timer& timer, TaskInformationTable& taskInformationTable) :
  m_timer(timer),
  m_taskInformationTable(taskInformationTable),
  m_resetDevice(callback(this, &BikeSystem::setReset)),
  m_gearSystemDevice(callback(this, &BikeSystem::setNewGear)) {
}
#elif defined(USE_EVENT_QUEUE)
BikeSystem::BikeSystem(Timer& timer, EventQueue& eventQueue) :
  m_timer(timer),
  m_eventQueueForISRs(eventQueue),
  m_resetDevice(callback(this, &BikeSystem::setReset)),
  m_gearSystemDevice(callback(this, &BikeSystem::setNewGear)) {
}
#elif defined(USE_MULTITASKING)
BikeSystem::BikeSystem() :
  m_resetDevice(callback(this, &BikeSystem::setReset)),
  m_gearSystemDevice(callback(this, &BikeSystem::setNewGear)),
  m_wheelCounterDevice(m_countQueue),
  m_lcdDisplay(m_processedMail) {

}
#endif

#if !defined(USE_MULTITASKING)
void BikeSystem::updateCurrentGear() {
  std::chrono::microseconds taskStartTime = m_timer.elapsed_time();
  
  // in case of event handling, check for a new gear
#if defined(USE_EVENT_HANDLING)  
  if (m_newGear) {
    m_gear = m_gearSystemDevice.getCurrentGear();
    m_newGear = false;
  }
#else
  m_gear = m_gearSystemDevice.getCurrentGear();
#endif

  logPeriodAndExecutionTime(GEAR_TASK_INDEX, taskStartTime);
}

void BikeSystem::updateWheelRotationCount() {
  std::chrono::microseconds taskStartTime = m_timer.elapsed_time();
  
  m_wheelRotationCount = m_wheelCounterDevice.getCurrentRotationCount();

  logPeriodAndExecutionTime(COUNT_TASK_INDEX, taskStartTime);
}
  
void BikeSystem::updateDisplay(int subTaskIndex) {
  std::chrono::microseconds taskStartTime = m_timer.elapsed_time();
  
  m_lcdDisplay.show(m_gear, m_wheelRotationCount, subTaskIndex);

  logPeriodAndExecutionTime(DISPLAY_TASK_INDEX, taskStartTime);
}

void BikeSystem::updateDisplayInOneTask() { 
  updateDisplay(-1);
}

void BikeSystem::checkAndPerformReset() {
  std::chrono::microseconds taskStartTime = m_timer.elapsed_time();
  
  // switch the next two lines for a polling or event-based handling of reset
#if defined(USE_EVENT_HANDLING)
  if (m_reset) {
    printf("Reset task (event): response time is %d usecs\n", (int) (m_timer.elapsed_time().count() - m_resetTime.count())); 
    m_wheelCounterDevice.reset();
    m_reset = false;
  }
#else
  if (m_resetDevice.checkReset()) {
    printf("Reset task: response time is %d usecs\n", (int) (m_timer.elapsed_time().count() - m_resetTime.count())); 
    m_wheelCounterDevice.reset();
  }
#endif

  logPeriodAndExecutionTime(RESET_TASK_INDEX, taskStartTime);
}
#endif // USE_MULTITASKING

#if defined(USE_EVENT_HANDLING)
void BikeSystem::setReset() {
  m_reset = true;
  m_resetTime = m_timer.elapsed_time();
#if defined(USE_RTC)
  // set the task to be ready to run if applicable
  // the task index should not be hardcoded here
  m_taskInformationTable.makeTaskReadyToRun(0);
#endif
#if defined(USE_EVENT_QUEUE)
  // defer the job to the event queue if applicable
  m_eventQueueForISRs.call(mbed::callback(this, &BikeSystem::checkAndPerformReset));
#endif
}
#endif

#if defined(USE_EVENT_HANDLING)
void BikeSystem::setNewGear() {
  m_newGear = true;
  // set the task to be ready to run if applicable
  // the task index should not be hardcoded here
#if defined(USE_RTC)
  m_taskInformationTable.makeTaskReadyToRun(1);
#endif

#if defined(USE_EVENT_QUEUE)
  // defer the job to the event queue if applicable
  m_eventQueueForISRs.call(mbed::callback(this, &BikeSystem::updateCurrentGear));
#endif
}
#endif

#if defined(USE_MULTITASKING)
void BikeSystem::start() {
  // start the timer
  m_timer.start();

  // the wheel counter and lcd display must be started
  // the reset and gear system device are event-based

  // start the wheel counter device
  m_wheelCounterDevice.start();

  // start the lcd display
  m_lcdDisplay.start();
  
  // start the processing thread
  m_processingThread.start(callback(this, &BikeSystem::processData));

  printf("Bike system started\n");

  // dispatch event on the ISR queue forever
  m_eventQueueForISRs.dispatch_forever();
}

void BikeSystem::processData() {
  // process data forever
  std::chrono::microseconds startTime = m_timer.elapsed_time();
  long totalRotationCount = 0;
  while (true) {
    // get the rotation count
    int rotationCount = 0;
    m_countQueue.try_get_for(Kernel::wait_for_u32_forever, (int**) &rotationCount);

    // get the elapsed time since last rotation count
    std::chrono::microseconds currentTime = m_timer.elapsed_time();
    std::chrono::microseconds elapsedSinceLastUpdate = currentTime - startTime;

    // compute the average speed
    totalRotationCount += rotationCount;
    // compute distance in m
    long distance = (WHEEL_CIRCUMFERENCE * totalRotationCount) / 1000;
    // compute time in seconds
    long totalElapsedTimeInSecs = elapsedSinceLastUpdate.count() / 1000000;
    // compute average speed in km/h
    float averageSpeed = (float) (distance * 1000) / (float) (totalElapsedTimeInSecs * 3600);
    // printf("Bike Average speed is %d.%d\n", (int) averageSpeed, (int) ((averageSpeed - (int) averageSpeed) * 100000));
    
    // push the new processed data
    ProcessedData* pProcessedData = m_processedMail.try_alloc_for(Kernel::wait_for_u32_forever);
    pProcessedData->averageSpeed = averageSpeed;
    m_processedMail.put(pProcessedData);
  }
}

#endif

#if !defined(USE_MULTITASKING)
void BikeSystem::logPeriodAndExecutionTime(int taskIndex, const std::chrono::microseconds& taskStartTime) {
  std::chrono::microseconds periodTime = taskStartTime - m_taskStartTime[taskIndex];  
  m_taskStartTime[taskIndex] = taskStartTime;
  std::chrono::microseconds taskEndTime = m_timer.elapsed_time();
  std::chrono::microseconds executionTime = taskEndTime - m_taskStartTime[taskIndex];  
  printf("%s task: period %d usecs execution time %d usecs start time %d usecs\n", 
         TASK_DESCRIPTORS[taskIndex], 
         (int) periodTime.count(),
         (int) executionTime.count(),
         (int) m_taskStartTime[taskIndex].count()); 
}
#endif

} // namespace

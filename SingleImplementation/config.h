#pragma once

// for static cyclic scheduling
//#define USE_STATIC_SCHEDULING

// for static cyclic scheduling with event handling
//#define USE_STATIC_SCHEDULING
//#define USE_EVENT_HANDLING

// for RTC scheduling (always with event handling)
//#define USE_EVENT_HANDLING
//#define USE_RTC

// for event queue with main thread only (always with event handling)
//#define USE_EVENT_HANDLING
//#define USE_EVENT_QUEUE

// for event queue with two main thread (always with event handling)
//#define USE_EVENT_HANDLING
//#define USE_EVENT_QUEUE
//#define USE_SECOND_THREAD

// for multitasking (always with event handling)
#define COUNT_QUEUE_SIZE 10
#define PROCESSED_DATA_QUEUE_SIZE 5
#define USE_EVENT_HANDLING
#define USE_MULTITASKING

//#include "mbed.h"

#if MBED_CONF_MBED_TRACE_ENABLE
#include "mbed_trace.h"
#define TRACE_GROUP "main"
#endif // MBED_CONF_MBED_TRACE_ENABLE

#include "update_client/USBSerialUC.h"

//#define USE_SINGLE_IMPLEMENTATION
#if ! defined(MBED_TEST_MODE)

#if ! defined(USE_SINGLE_IMPLEMENTATION)

#include "StaticScheduling/BikeSystem.h"
//#include "StaticSchedulingWithEvent/BikeSystem.h"
//#include "RTC/BikeSystem.h"
//#include "WithEventQueue/BikeSystem.h"
//#include "WithMultiTasking/BikeSystem.h"
//#include "WithMultiTasking_Memory/BikeSystem.h"
//#include "WithSensor/BikeSystem.h"

int main() {
  mbed_trace_init();
  
  tr_info("Built: %s, %s\n", __DATE__, __TIME__);
  // launch the updater
  //update_client::USBSerialUC usbSerialUpdateClient;
  //usbSerialUpdateClient.start();
  //tr_info("Update client started");

  tr_info("Bike computer program started");
  
  static_scheduling::BikeSystem bikeSystem;
  //static_scheduling_with_event::BikeSystem bikeSystem;
  //rtc::BikeSystem bikeSystem;
  //with_event_queue::BikeSystem bikeSystem;
  //with_multitasking::BikeSystem bikeSystem;
  //with_sensor::BikeSystem bikeSystem;
  //with_multitasking_memory::BikeSystem bikeSystem;
  bikeSystem.start();
}

#else // USE_SINGLE_IMPLEMENTATION

#include "SingleImplementation/config.h"
#include "SingleImplementation/BikeSystem.h"

#if defined(USE_STATIC_SCHEDULING)
// main() runs in its own thread in the OS
int main() {  
  mbed_trace_init();
  
 #if defined(USE_EVENT_HANDLING)
  tr_info("Starting Super-Loop with event handling");
#else
  tr_info("Starting Super-Loop with no event");
#endif

  // create a timer for measuring performance
  Timer timer;

  // start the timer
  timer.start();

  // create the bike system representing the bike with its devices
  single_implementation::BikeSystem bikeSystem(timer);

  while (true) {
    // register the time at the beginning of the cyclic schedule period
    std::chrono::microseconds startTime = timer.elapsed_time();

    // perform tasks as documented in the timetable
    bikeSystem.updateCurrentGear();
    bikeSystem.updateWheelRotationCount();
    bikeSystem.updateDisplay(0);
    bikeSystem.checkAndPerformReset();
    bikeSystem.updateWheelRotationCount();
    bikeSystem.updateDisplay(1);
    bikeSystem.updateCurrentGear();
    bikeSystem.updateWheelRotationCount();
    bikeSystem.updateDisplay(2);
    bikeSystem.checkAndPerformReset();
    bikeSystem.updateWheelRotationCount();
    
    ThisThread::sleep_for(std::chrono::milliseconds(100));
    
    // register the time at the end of the cyclic schedule period and print the elapsed time for the period
    std::chrono::microseconds endTime = timer.elapsed_time();
    printf("Elapsed time is %d milliseconds\n", (int) (endTime.count() - startTime.count())/1000);
  }
}
#elif defined(USE_RTC)
// main() runs in its own thread in the OS
int main() {  
  mbed_trace_init();
  
  tr_info("Starting with RTC scheduler");

  // create a timer for measuring performance
  Timer timer;

  // start the timer
  timer.start();

  // create the task information table
  TaskInformationTable taskInformationTable;

  // create the bike system representing the bike with its devices
  single_implementation::BikeSystem bikeSystem(timer, taskInformationTable);

  // Add the tasks by priority order.
  // The task with the highest priority is the push reset task.
  // This task is not periodic and made ready to run in the setResetTaskToReady() function.
  taskInformationTable.addTask(&bikeSystem, &BikeSystem::checkAndPerformReset);
  // The next task is the task for updating the gear.
  // This task is not periodic and made ready to run in the setUpdateGearTaskToReady() function.
  taskInformationTable.addTask(&bikeSystem, &BikeSystem::updateCurrentGear);
  // the task with the next priority is the one for updating the current rotation count (periodic task)
  taskInformationTable.addTask(&bikeSystem, &BikeSystem::updateWheelRotationCount, WheelCounterDevice::TASK_PERIOD);  
  // the task with the next priority is the one for updating the LCD display (period task)
  taskInformationTable.addTask(&bikeSystem, &BikeSystem::updateDisplayInOneTask, LCDDisplay::TASK_PERIOD);
  
  int nbrOfTasks = taskInformationTable.getNbrOfTasks();
  printf("Nbr of tasks in scheduler is %d\n", nbrOfTasks);

  // run a RTC scheduling  
  while (true) {
    // wait for a task to be ready to run
    taskInformationTable.waitForTaskReadyToRun();
    
    // tasks are ordered by priority
    // run the first ready to run task by priority order
    for (int taskIndex = 0; taskIndex < nbrOfTasks; taskIndex++) {
      if (taskInformationTable.isReadyToRun(taskIndex)) {
        // run the task
        taskInformationTable.execute(taskIndex);
        // note that the task ready to run status is reset after execution
        break;
      }
    }
  }
}
#elif defined(USE_EVENT_QUEUE) && !defined(USE_SECOND_THREAD)
// main() runs in its own thread in the OS
int main() {  
  mbed_trace_init();
  
  tr_info("Starting with EventQueue");

  // create a timer for measuring performance
  Timer timer;

  // start the timer
  timer.start();

  // The EventQueue has no concept of event priority. 
  // If you schedule events to run at the same time, the order in which the events run relative to one another is undefined. 
  // The EventQueue only schedules events based on time.
  // If you want to separate your events into different priorities, you must instantiate an EventQueue for each priority. 
  // You must appropriately set the priority of the thread dispatching each EventQueue instance.
  // This version does not explicetly create threads and uses a single queue.
  EventQueue eventQueue;
  
  // create the bike system representing the bike with its devices
  single_implementation::BikeSystem bikeSystem(timer, eventQueue);

  // schedule periodic events
  eventQueue.call_every(WheelCounterDevice::TASK_PERIOD, callback(&bikeSystem, &BikeSystem::updateWheelRotationCount));
  eventQueue.call_every(LCDDisplay::TASK_PERIOD, callback(&bikeSystem, &BikeSystem::updateDisplayInOneTask));

  // dispatch events in the queue forever
  eventQueue.dispatch_forever();

  return 0;
}
#elif defined(USE_EVENT_QUEUE) && defined(USE_SECOND_THREAD)
// main() runs in its own thread in the OS
int main() {  
  mbed_trace_init();
  
  tr_info("Starting with EventQueue and threads");

  // create a timer for measuring performance
  Timer timer;

  // start the timer
  timer.start();

  // The EventQueue has no concept of event priority. 
  // If you schedule events to run at the same time, the order in which the events run relative to one another is undefined. 
  // The EventQueue only schedules events based on time.
  // If you want to separate your events into different priorities, you must instantiate an EventQueue for each priority. 
  // You must appropriately set the priority of the thread dispatching each EventQueue instance.
  EventQueue eventQueuePeriodic;
  EventQueue eventQueueAperiodic;

  // create the bike system representing the bike with its devices
  single_implementation::BikeSystem bikeSystem(timer, eventQueueAperiodic);

  // create a thread with higher priority for hanlding aperiodic events
  Thread aperiodicEventThread(osPriorityAboveNormal);
  aperiodicEventThread.start(callback(&eventQueueAperiodic, &EventQueue::dispatch_forever));
    
  // schedule periodic events
  eventQueuePeriodic.call_every(WheelCounterDevice::TASK_PERIOD, callback(&bikeSystem, &BikeSystem::updateWheelRotationCount));
  eventQueuePeriodic.call_every(LCDDisplay::TASK_PERIOD, callback(&bikeSystem, &BikeSystem::updateDisplayInOneTask));

  // dispatch events in the queue forever
  eventQueuePeriodic.dispatch_forever();

  return 0;
}
#elif defined(USE_MULTITASKING)
// main() runs in its own thread in the OS
int main() {  
  mbed_trace_init();
  
  tr_info("Starting with Multitasking");

  // create the bike system representing the bike with its devices
  single_implementation::BikeSystem bikeSystem;
  bikeSystem.start();
}
#endif

#endif // USE_SINGLE_IMPLEMENTATION

#endif // !MBED_TEST_MODE
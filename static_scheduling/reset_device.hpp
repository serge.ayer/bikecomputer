#pragma once

#include "mbed.h"
#include <chrono>

namespace static_scheduling {

class ResetDevice {
public:
  // constructor 
  ResetDevice(Timer& timer);

  // method called for checking the reset status
  bool checkReset();
 
  // for computing the response time
  const std::chrono::microseconds& getFallTime();

private:
  // called when the button is pressed
  void onFall();

  // definition of task execution time
  static constexpr std::chrono::microseconds TASK_RUN_TIME = 100000us;

  // data members  
  // instance representing the reset button
  InterruptIn m_resetButton;
  Timer& m_timer;
  std::chrono::microseconds m_fallTime;
};

} // namespace static_scheduling

#include "GearSystemDevice.h"

#include "mbed_trace.h"

#if MBED_CONF_MBED_TRACE_ENABLE
#define TRACE_GROUP "GearSystemDevice"
#endif // MBED_CONF_MBED_TRACE_ENABLE

namespace static_scheduling {

GearSystemDevice::GearSystemDevice() :
  m_usbSerial(true) {
  m_thread.start(callback(this, &GearSystemDevice::read));
}

void GearSystemDevice::read() {
  while (true) {
    if (m_usbSerial.available()) {
      char str[2] = { (char) m_usbSerial.getc(), 0};
      m_currentGear = atoi(str);
      tr_info("New gear set to %d", m_currentGear);
    }
    ThisThread::sleep_for(READING_RATE);
  }
}

int GearSystemDevice::getCurrentGear() {
  // simulate task computation by waiting for the required task run time
  wait_us(TASK_RUN_TIME.count());
  return m_currentGear;
}

} // namespace
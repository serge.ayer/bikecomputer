#include "ResetDevice.h"
#include <chrono>

#if defined(TARGET_DISCO_L475VG_IOT01A) 
#define PUSH_BUTTON BUTTON1
#define POLARITY_PRESSED 0
#elif defined(TARGET_EP_AGORA)
#define PUSH_BUTTON PIN_NAME_PUSH_BUTTON
#define POLARITY_PRESSED ACTIVE_LOW_POLARITY
#endif

namespace static_scheduling {

ResetDevice::ResetDevice(Timer& timer) :
  m_timer(timer),
  m_resetButton(PUSH_BUTTON) {
  // register a callback for computing the response time
  m_resetButton.fall(callback(this, &ResetDevice::onFall));
}

void ResetDevice::onFall() {
  m_fallTime = m_timer.elapsed_time();
}

const std::chrono::microseconds& ResetDevice::getFallTime() {
  return m_fallTime;
}

bool ResetDevice::checkReset() {
  // simulate task computation by waiting for the required task run time
  wait_us(TASK_RUN_TIME.count());
 
  return m_resetButton.read() == POLARITY_PRESSED;
}

} // namespace static_scheduling


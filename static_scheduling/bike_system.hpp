#pragma once

#include "gear_system_device.hpp"
#include "WheelCounterDevice.h"
#include "ResetDevice.h"
#include "LCDDisplay.h"

namespace static_scheduling {

class BikeSystem {
public:
    BikeSystem();

    void start();

private:
    void updateCurrentGear();
    void updateWheelRotationCount();

    void checkAndPerformReset();
    void updateDisplay(int subTaskIndex);

    // number of wheel rotation (read from the device)
    int m_wheelRotationCount = 0;
    // current gear (read from the device)
    int m_gear = 0;
    // data member that represents the device for manipulating the gear
    GearSystemDevice m_gearSystemDevice;
    // data member that represents the device for counting wheel rotations
    WheelCounterDevice m_wheelCounterDevice;
    // data member that represents the device used for resetting
    ResetDevice m_resetDevice;
    // data member that represents the device display
    LCDDisplay m_lcdDisplay;

    void logPeriodAndExecutionTime(int taskIndex, const std::chrono::microseconds &taskStartTime);
    Timer m_timer;
    static constexpr int NBR_OF_TASKS = 4;
    static constexpr int RESET_TASK_INDEX = 0;
    static constexpr int GEAR_TASK_INDEX = 1;
    static constexpr int COUNT_TASK_INDEX = 2;
    static constexpr int DISPLAY_TASK_INDEX = 3;
    static constexpr char *TASK_DESCRIPTORS[] = { (char *) "Reset", (char *) "Gear", (char *) "Count", (char *) "Display"};
    std::chrono::microseconds m_taskStartTime[NBR_OF_TASKS];
};

} // namespace static_scheduling
